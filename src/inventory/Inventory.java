/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inventory;

import java.util.ArrayList;
import java.util.Scanner;
/**
 *
 * @author Horatio grant
 */

public class Inventory {
    Scanner input = new Scanner(System.in);
    private ArrayList <Item> itemList = new ArrayList<Item>();
    public static int count;
    public Inventory(){
    
}
    public void addItem(){
        System.out.println("Enter name of the item to add:");
        String name = input.nextLine();
        
        System.out.println("Enter the quantity:");
        int quantity = input.nextInt();
        itemList.add(new Item(count++, quantity, name));
    } 
    public void newItem(int quantity,String name ){
        itemList.add(new Item(count++, quantity, name));
    }
    
    public void printInventory(){
        for (int x=0; x<itemList.size();x++){
            itemList.get(x).itemInfo();
           
        }
    }
    public void findItem(){
         System.out.println("Enter ID of the item whose quantity you want to find:");
         int temp_ID = input.nextInt();
        System.out.print("Item's quantity is: ");
        System.out.print(itemList.get(temp_ID).getQuantity());
       
    }
   
    
}
