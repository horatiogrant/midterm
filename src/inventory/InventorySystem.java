package inventory;

import java.util.Scanner;

/**
 * Simulator to add items, print inventory and to search item's quantity.
 *
 * @author Horatio Grant
 * @author Paul Bonenfant
 */
public class InventorySystem {

    public static void main(String[] args) {
        
        //Item item1 = new Item(0, 0, null);//No ID, quantity or name assigned.
        Inventory in = new Inventory ();
        /**
         * Add a new item to the inventory. Program asks the user to enter name
         * and quantity of the item. For new item's ID, program simply
         * increments the ID of the last item added.
         */
        Scanner input = new Scanner(System.in);
        
        in.addItem();

        /**
         * Add two more items.
         */
  
      in.newItem(5, "ET-2750 printer");


      in.newItem(10, "MX-34 laptops");

        /**
         * Print the inventory.
         */
        in.printInventory();

        /**
         * Find quantity of a particular item in the inventory.
         */
        
        in.findItem();

    }

}
