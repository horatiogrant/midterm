package inventory;

/**
 * A class to model items stored in the inventory of a Warehouse. For simplicity, let's assume each item in the
 * inventory has a name, quantity and a unique ID. The system allows to add new items, find quantity by item's ID and
 * print full inventory. Use this code to answer Q#16 and Q#17 of Part B.
 *
 * @author Horatio Grant
 */
public class Item {

    private int itemID;
    private String name;
    private int quantity;

    public static Item[] inventory = new Item[100];
   

    /**
     * Constructor takes in item's ID, quantity and it's name.
     *
     * @param itemID Unique ID of the item.
     * @param quantity the quantity of the item.
     * @param name the name of the item.
     */
    public Item(int itemID, int quantity, String name) {
        this.itemID = itemID;
        this.quantity = quantity;
        this.name = name;
    }

    public void itemInfo(){
     System.out.println("ID: " + getId()
                    + "\t Name: " + getName()
                    + "\t Quantity:" + getQuantity());   
    }
    public int getId(){
        return itemID;
    }
    public String getName(){
        return name;
    }
    public int getQuantity(){
        return quantity;
    }

    

}
